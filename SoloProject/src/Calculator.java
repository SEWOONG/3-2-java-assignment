import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.util.Stack;

/*
 계산기
 미완성 
 */


public class Calculator extends JFrame{
	//변수들 선언
	private String str = "";
	private int previous = 0, temp = 0, result = 0;
	private int operator;
	
	JButton[] number = {new JButton("0"), new JButton("1"), new JButton("2"), new JButton("3"), 
			new JButton("4"), new JButton("5"), new JButton("6"), new JButton("7"), 
			new JButton("8"), new JButton("9")};
	
	JButton[] jb = {new JButton("취소"), new JButton("계산"), new JButton("+"), 
			new JButton("-"), new JButton("*"), new JButton("/")};
	
	JTextField jt = new JTextField(15);
	
	//계산기 중심부
	public Calculator() {
		setTitle("계산기");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container c = getContentPane();
		c.setLayout(new BorderLayout(0,0));
		c.add(new NorthBorder(), BorderLayout.NORTH);
		c.add(new CenterBorder(), BorderLayout.CENTER);
		
		setSize(300,300);
		setVisible(true);
	}
	
	
	//북쪽 
	public class NorthBorder extends JPanel {
		public NorthBorder() {
			setLayout(new FlowLayout());
			setBackground(Color.GRAY);
			add(new JLabel("수식 입력"));
			jt.setText(str);
			add(jt);
		}
	}
	
	//중간
	public class CenterBorder extends JPanel {
		public CenterBorder() {
			setLayout(new GridLayout(4,4,2,2));
			setBackground(Color.BLACK);
			
			for(int i = 0; i < 10; i++) {
				number[i].addActionListener(new MyActionListener());
				add(number[i]);
			}
			
			jb[0].addActionListener(new MyActionListener());
			jb[1].addActionListener(new MyActionListener());
			add(jb[0]);
			add(jb[1]);
			
			for(int i = 2; i < 6; i++) {
				jb[i].setBackground(Color.GREEN);
				jb[i].addActionListener(new MyActionListener());
				add(jb[i]);
			}
		}
	}
	
	public class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JButton j = (JButton)e.getSource();
			
			String s = j.getText();
			char c = s.charAt(0);
			str += s;
			
			if(s == "+") {
				operator = 1;
				return;
			}
			else if(s == "-") {
				operator = 2;
				return;
			}
			else if(s == "*") {
				operator = 3;
				return;
			}
			else if(s == "/") {
				operator = 4;
				return;
			}
			else if(s == "취소") {
				str = "";
				jt.setText(str);
				temp = 0;
				previous = 0;
				result = 0;
				return;
			}
			else if(s == "계산") {
				str = Integer.toString(result);
				jt.setText(str);
				
				str = "";
				previous = 0;
				temp = 0;
				result = 0;
				return;
			}
			else {
				temp = c - '0';
				
				//연산 
				if(operator != 0) {
					switch(operator) {
					case 1:
						result = result + temp;
						break;
					case 2:
						result = result - temp;
						break;
					case 3:
						result = result * temp;
						break;
					case 4:
						result = result / temp;
						break;
						default:
							break;
							
					}
					operator = 0;
				}
				else {
					//연속적으로 숫자가 들어올 때
					if(previous != 0) {
						if(previous >= 1000) {
							previous *= 10;
							temp = previous + temp;
						}
						else if(previous >= 100) {
							previous *= 10;
							temp = previous + temp;
						}
						else if(previous >= 10) {
							previous *= 10;
							temp = previous + temp;
						}
						else {
						previous *= 10;
						temp = previous + temp;
						}
					}
					result = temp;
				}
				
				
				//이전 값
				if(temp != 0) {
					previous = temp;
				}
				else {
					previous = (int)c;
				}
				
			}
				
			
			jt.setText(str);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Calculator();
	}

}
