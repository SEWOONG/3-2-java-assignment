package Assignment;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
 * '+'를 누르면 글씨가 크게, '-'를 누르면 글씨가 작아지게 하기
 */

public class Assignment3 extends JFrame {
	private JLabel jl = new JLabel("Love Java");
	Font f = jl.getFont();
	int size = f.getSize() + 5;
	
	public Assignment3() {
		setTitle("+,-키로 폰트 크기 조절");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container c = getContentPane();
		c.setLayout(new FlowLayout());
		
		jl.setFont(new Font("TimesRoman", Font.PLAIN, size));
		jl.setLocation(110,10);
		
		c.setFocusable(true);
		c.requestFocus();
		c.addKeyListener(new TransSize());
		
		c.add(jl);
		setSize(300,300);
		setVisible(true);
	}
	
	public class TransSize extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			if((char)e.getKeyChar() == '+')
				size += 5;
			else if((char)e.getKeyChar() == '-')
				size -= 5;
			
			jl.setFont(new Font("TimesRoman", Font.PLAIN, size));
		}
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Assignment3();
	}

}