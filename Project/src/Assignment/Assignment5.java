package Assignment;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
 * 금액을 입력하면 5만원, 1만원, 1000원, 500원, 50원, 10원, 1원 짜리 동전 최소 몇 개로 이루어지는지
 */

public class Assignment5 extends JFrame {
	JLabel jl[] = {new JLabel("금액"), new JLabel("오만원"), new JLabel("만원"), new JLabel("천원"), new JLabel("500원"), 
			new JLabel("100원"), new JLabel("50원"), new JLabel("10원"), new JLabel("1원")};
	JTextField jt[] = new JTextField[9];
	JButton jb = new JButton("계산");
	JCheckBox jc[] = new JCheckBox[7];
	int money[] = {50000, 10000, 1000, 500, 100, 50, 10};
	boolean isCheck[] = new boolean[7];
	
	public Assignment5() {
		int labelX = 70, labelY = 50;
		int textX = 120, textY = 50;
		
		setTitle("Money Changer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Container c = getContentPane();
		c.setBackground(Color.PINK);
		c.setLayout(null);
		
		for(int i = 0; i < 9; i++) {
			jt[i] = new JTextField(10);
		}
		for(int i = 0; i < 7; i++) {
			jc[i] = new JCheckBox();
		}
		
		
		//금액
		jl[0].setSize(30,30);
		jl[0].setLocation(50,18);
		c.add(jl[0]);
		
		jt[0].setSize(100,22);
		jt[0].setLocation(100,20);
		c.add(jt[0]);
		
		jb.setSize(60,22);
		jb.setLocation(210,20);
		jb.addMouseListener(new Exchange());
		c.add(jb);
		
		//오만원 ~ 1원
		for(int i = 1; i < 9; i++) {
			jl[i].setSize(50,30);
			jl[i].setLocation(labelX, labelY);
			c.add(jl[i]);
			
			jt[i].setSize(80,22);
			jt[i].setLocation(textX, textY);
			c.add(jt[i]);
			
			if(!(i == 0 || i == 8)) {
				jc[i-1].setSize(30,30);
				jc[i-1].setLocation(textX + 90, textY - 5);
				jc[i-1].addItemListener(new IsCheck(i-1));
				jc[i-1].setHorizontalAlignment(JCheckBox.CENTER);
				c.add(jc[i - 1]);
			}
			labelY += 23;
			textY += 23;
		}
		
		
		setSize(300,330);
		setVisible(true);
		
		c.setFocusable(true);
		c.requestFocus();
	}
	
	public class IsCheck implements ItemListener {
		int index;
		public IsCheck(int index) {
			this.index = index;
		}
		
		public void itemStateChanged(ItemEvent e) {
			if(e.getStateChange() == ItemEvent.SELECTED)
				isCheck[index] = true;
			else
				isCheck[index] = false;
		}
	}
	
	public class Exchange extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			JButton b = (JButton)e.getSource();
			String s = b.getText();
			
			//각각 나눈 값 넣기
			if(s == "계산") {
				int num = Integer.parseInt(jt[0].getText());
				for(int i = 0; i < 8; i++) {
					if(i == 7) {
						jt[i+1].setText(String.valueOf(num / 1));
						break;
					}
					
					if(isCheck[i] == true) {
						String temp = String.valueOf(num / money[i]);
						jt[i+1].setText(temp);
						num %= money[i];
					}
					else {
						jt[i+1].setText("0");
					}
				}
			}
		}
	}
	
	public static void main(String[] args) {
		new Assignment5();
	}
}
