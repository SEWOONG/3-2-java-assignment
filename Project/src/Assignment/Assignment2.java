package Assignment;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
 * 마우스를 올리면 "Love Java", 내리면 "사랑해"가 나오게 하기
 */

public class Assignment2 extends JFrame {
	private JLabel jl = new JLabel("사랑해");

	public Assignment2() {
		setTitle("마우스 올리기 내리기 연습");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container c = getContentPane();
		c.setLayout(new FlowLayout());

		c.add(jl);

		jl.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				JLabel j = (JLabel)e.getComponent();
				j.setText("Love Java");
			}
			public void mouseExited(MouseEvent e) {
				JLabel j = (JLabel)e.getComponent();
				j.setText("사랑해");
			}
		});

		setSize(300,250);
		setVisible(true);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Assignment2();
	}
}