package Assignment;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
 * 금액을 입력하면 5만원, 1만원, 1000원, 500원, 50원, 10원, 1원 짜리 동전 최소 몇 개로 이루어지는지
 */

public class Assignment4 extends JFrame {
	JLabel jl[] = {new JLabel("금액"), new JLabel("오만원"), new JLabel("만원"), new JLabel("천원"), new JLabel("500원"), 
			new JLabel("100원"), new JLabel("50원"), new JLabel("10원"), new JLabel("1원")};
	JTextField jt[] = new JTextField[9];
	JButton jb = new JButton("계산");
	
	public Assignment4() {
		int labelX = 70, labelY = 50;
		int textX = 120, textY = 50;
		
		setTitle("Money Changer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Container c = getContentPane();
		c.setBackground(Color.PINK);
		c.setLayout(null);
		
		for(int i = 0; i < 9; i++) {
			jt[i] = new JTextField(10);
		}
		
		//금액
		jl[0].setSize(30,30);
		jl[0].setLocation(50,18);
		c.add(jl[0]);
		
		jt[0].setSize(100,22);
		jt[0].setLocation(100,20);
		c.add(jt[0]);
		
		jb.setSize(60,22);
		jb.setLocation(210,20);
		jb.addMouseListener(new Exchange());
		c.add(jb);
		
		//오만원 ~ 1원
		for(int i = 1; i < 9; i++) {
			jl[i].setSize(50,30);
			jl[i].setLocation(labelX, labelY);
			c.add(jl[i]);
			
			jt[i].setSize(80,22);
			jt[i].setLocation(textX, textY);
			c.add(jt[i]);
			
			labelY += 23;
			textY += 23;
		}
		
		
		setSize(300,330);
		setVisible(true);
		
		c.setFocusable(true);
		c.requestFocus();
	}
	
	public class Exchange extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			JButton b = (JButton)e.getSource();
			String s = b.getText();
			
			//각각 나눈 값 넣기
			if(s == "계산") {
				int num = Integer.parseInt(jt[0].getText());
				jt[1].setText(String.valueOf(num / 50000));
				num %= 50000;
				jt[2].setText(String.valueOf(num / 10000));
				num %= 10000;
				jt[3].setText(String.valueOf(num / 10000));
				num %= 10000;
				jt[3].setText(String.valueOf(num / 1000));
				num %= 1000;
				jt[4].setText(String.valueOf(num / 500));
				num %= 500;
				jt[5].setText(String.valueOf(num / 100));
				num %= 100;
				jt[6].setText(String.valueOf(num / 50));
				num %= 50;
				jt[7].setText(String.valueOf(num / 10));
				num %= 10;
				jt[8].setText(String.valueOf(num / 1));
				num %= 1;
			}
		}
	}
	
	public static void main(String[] args) {
		new Assignment4();
	}
}
