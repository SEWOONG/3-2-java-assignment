package Assignment;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
 * 한 번 클릭 시 원을 0.4초마다 이동시키기
 */

public class Assignment6 extends JFrame {
	private MyPanel tr = new MyPanel();
	
	
	public Assignment6() {
		setTitle("원을 0.4초 간격으로 랜덤하게 이동시키기");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setContentPane(tr);
		
		setSize(300,300);
		setVisible(true);
		this.setFocusable(true);
		this.requestFocus();
	}
	
	class MyPanel extends JPanel implements Runnable{
		int x;
		int y;
		
		public MyPanel() {
			addMouseListener(new MouseAdapter() {
				public void mousePressed(MouseEvent e) {
					Thread th = new Thread(tr);
					th.start();
				}
			});
		}
		
		@Override
		public void run() {
			while(true) {
				try {
					Thread.sleep(400);
				}
				catch(Exception e) {
					return;
				}
				
				x = (int) (Math.random() * this.getWidth());
				y = (int) (Math.random() * this.getHeight());
				repaint();
			}
		}
		
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(Color.MAGENTA);
			g.drawArc(x, y, 50, 50, 0, 360);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Assignment6();
	}

}
