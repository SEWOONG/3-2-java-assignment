package Assignment;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/*
 *계산기 GUI 만드는 문제
 */

public class Assignment1 extends JFrame{
	//변수들 선언
	private String str = "";
	
	JButton[] number = {new JButton("0"), new JButton("1"), new JButton("2"), new JButton("3"), 
			new JButton("4"), new JButton("5"), new JButton("6"), new JButton("7"), 
			new JButton("8"), new JButton("9")};
	
	JButton[] jb = {new JButton("CE"), new JButton("계산"), new JButton("+"), 
			new JButton("-"), new JButton("*"), new JButton("/")};
	
	JTextField jt = new JTextField(15);
	JTextField jt2 = new JTextField(15);
	
	//계산기 중심부
	public Assignment1() {
		setTitle("계산기 프레임");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container c = getContentPane();
		c.setLayout(new BorderLayout(0,0));
		c.add(new NorthBorder(), BorderLayout.NORTH);
		c.add(new CenterBorder(), BorderLayout.CENTER);
		c.add(new UnderBorder(), BorderLayout.SOUTH);
		
		setSize(300,300);
		setVisible(true);
	}
	
	
	//북쪽 
	public class NorthBorder extends JPanel {
		public NorthBorder() {
			setLayout(new FlowLayout());
			setBackground(Color.GRAY);
			add(new JLabel("수식 입력"));
			jt.setText(str);
			add(jt);
		}
	}
	
	//중간
	public class CenterBorder extends JPanel {
		public CenterBorder() {
			setLayout(new GridLayout(4,4,2,2));
			setBackground(Color.BLACK);
			
			for(int i = 0; i < 10; i++) {
				number[i].addActionListener(new MyActionListener());
				add(number[i]);
			}
			
			for(int i = 0; i < 2; i++) {
				jb[i].addActionListener(new MyActionListener());
				add(jb[i]);
			}
			
			for(int i = 2; i < 6; i++) {
				jb[i].setBackground(Color.CYAN);
				jb[i].addActionListener(new MyActionListener());
				add(jb[i]);
			}
		}
	}
	
	//밑
	public class UnderBorder extends JPanel {
		public UnderBorder() {
			setLayout(new FlowLayout());
			setBackground(Color.YELLOW);
			
			add(new JLabel("계산 결과"));
			add(jt2);
			
		}
	}
	
	public class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JButton j = (JButton)e.getSource();
			String s = j.getText();
			
			if(s == "CE") {
				str = " ";
			}
			else if(s == "계산") {
				jt2.setText(str);
				str = " ";
			}
			else {
				str += s;
			}
			
			jt.setText(str);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Assignment1();
	}

}

